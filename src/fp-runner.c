/*!
 * @file fp-runner.c
 * @page fp-runner
 * @brief Исходный код программы, связывающий выходной поток find_particles и plot.py
 * @section synopsis SYNOPSIS
 * ./fp-runner image
 * @section author AUTHOR
 * Daniel Zagaynov
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
/*!
 * @brief Вызывает find_perticles, помещая его выходной поток в канал, позднее связывая его с входном потоком plot.py, аналогично вызываемой.
 * @param argc Количество аргументов командной строки
 * @param argv Аргументы командной строки
 * @return 0 В случае успешного завершения работы программы.
 */
int main(int argc, char* argv[]){
    int fd[2], status_1 = 0, status_2 = 0;
    pid_t img = 0, plot = 0;
    
    if(pipe(fd) == -1)
    {
        perror("pipe: ");
        return 1;
    }
    img = fork();

    if(img == -1)
    {
        close(fd[0]);
        close(fd[1]);
        perror("image fork: ");
        return 2;
    }
    else if(img == 0)
    {
        close(fd[0]);
        if(dup2(fd[1], 1) == -1)
        {
            close(fd[1]);
            perror("dup2: ");
            return 2;
        }
        execlp("./find_particles", "./find_particles", argv[1], NULL);
        perror("find_particles execlp: ");
        close(fd[1]);
        return 2;
    }

    plot = fork();
    close(fd[1]);

    if(plot == -1)
    {
        close(fd[0]);
        perror("plot fork:");
        return 3;
    }
    else if(plot == 0)
    {
        if(dup2(fd[0], 0) == -1)
        {
            close(fd[0]);
            perror("dup2: ");
            return 3;
        }
        
        execlp("/usr/bin/python3", "/usr/bin/python3", "fp-plot.py", NULL);
        perror("execlp: ");
        close(fd[0]);
        return 3;
    }
    wait(&status_1);
    if(WEXITSTATUS(status_1) != 0){
        kill(plot, SIGKILL);
        return 1;
    }
    else wait(&status_2);

    return 0;
}
