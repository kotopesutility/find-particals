"""!
    @file fp-runner.py
    @page fp-runner.py
    @brief Исходный код программы, связывающей выходной поток find_particles с входным потоком plot.py.
    @section synopsis SYNOPSIS
    python3 fp-runner.py image
    @section author AUTHOR
    Daniel Zagaynov
    ---------------
    @var str command_img
    @brief Содержит название программы для подсчета концентрации частиц на снимке.
    @var str command_plot
    @brief Содержит название программы для построения графиков.
    @var str command_python
    @brief Содержит название программы для запуска python3.
    @var subprocess.CompletedProcess img
    @var str contents
    @brief Содержит выходной поток процесса.
    @var subprocess.CompletedProcess plot
"""

import sys
import subprocess

command_img = './find_particles' 
command_plot = 'fp-plot.py'

command_python = 'python3'
if sys.platform in ['win32', 'cygwin']:
    command_python = 'py'
try:
    img = subprocess.Popen((command_img, sys.argv[1]), stdout=subprocess.PIPE)
except:
    command_img = 'find_particles.exe' 
    img = subprocess.Popen((command_img, sys.argv[1]), stdout=subprocess.PIPE)

contents = img.stdout.read()

plot = subprocess.Popen((command_python, command_plot), stdin = subprocess.PIPE)

plot.communicate(contents)
