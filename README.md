Данный проект позволяет определить распределение частиц по размерам, распознавая их на изображении. 

Для сборки проекта выполните следующие действия:
1. mkdir builddir
2. cd builddir
3. cmake -GNinja ../
4. ninja

После сборки:
1. cd bin
2. python3 main.py ../../tests/rabbit.png
или ./find_particles_runner ../../tests/rabbit.png

Для установки:
ninja install
