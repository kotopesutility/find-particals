/*!
 \file find_particles.cpp
 \page find_particles
 \section description DESCRIPTION
 \brief Исходной код программы, распознающей концентрацию частиц
 на снимке фильтра.
 \details
 Данная программа получает на вход изображение с частицами и определяет
 концентрацию частиц на нем, используя библиотеку OpenCV. 
 Результатом работы данной программы является набор чисел 
 {размер_частицы}:{кол-во таких частиц}, подаваемый на стандартный поток вывода.
 \section synopsis SYNOPSIS
 ./find_particles image
 \section author AUTHOR
 Daniel Zagaynov
 */


#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;

/*!
 *\brief Подсчитывает сумму пикселей на участке изображения
 * \param img Участок изображения
 * \param size Размер изображения
 * \return Сумму пикселей на изображении
 */
int summ(Mat img, int size){
    int total = 0;
    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            total += img.at<uchar>(i, j);
        }
    }
    return total;
}
/*!
 *\brief Проверяет, остались ли черные пиксели на изображении.
 * \param img Фрагмент изображения.
 *\return Истину, если на изображении не осталось черных пикселей. Ложь в ином случае.
 */
bool check_if_empty(Mat img){
    for(int i = 0; i < img.cols; i++){
        for(int j = 0; j < img.rows; j++){
            if( (int) img.at<uchar>(i, j) == 0) return false;
        }
    }
    return true;
}

/*!
 * \brief Проверяет, попала ли в рассматриваемый фрагмент изображения фигура, состоящая из нечетного количества пикселей.
 * \param img Фрагмент изображения.
 * \return Истину, если в рассматриваевом фрагменте изображения осталась фигура в своем начале имеющим нечетное количество пикселей.
 */
bool check_if_non_odd_square(Mat img){
    for(int i = 0; i < 6; i++){
        if((int) img.at<uchar>(0, i) * (int) img.at<uchar>(i, 0) * (int) img.at<uchar>(5, i) * (int) img.at<uchar>(i, 5) == 0) return false;
    }
    if (summ(img(Rect(1, 1, 4, 4)), 4) <= 255 * 15){
        for(int i = 1; i < 5; i++){
            for(int j = 1; j < 5; j++){
                img.at<uchar>(i, j) = 255;
            }
        }
        return true;
    }
    return false;
    }

/*!
 * \brief Проверяет, попала ли в рассматриваемый фрагмент изображения фигура, состоящая из четного количества пикселей.
 * \param img Фрагмент изображения.
 * \return Истину, если в рассматриваевом фрагменте изображения осталась фигура в своем начале имеющим четное количество пикселей.
 */
bool check_if_odd_square(Mat img){
    if (summ(img, 4) == (4 * 4 - 4) * 255 && ! (int) img.at<uchar>(1, 1) && ! (int) img.at<uchar>(1, 2) && ! (int) img.at<uchar>(2, 1)  && ! (int) img.at<uchar>(2, 2)){
        return true;
    }
    return false;
}
/*!
 * \brief Подсчитывает количество фигур, имеющих в своем начале либо четное, либо нечетное количество пикселей, за 1 шаг до их исчезновения
 * \param img Фрагмент изображения.
 * \param *odd Указатель на переменную, содержащую количество фигур, имеющих в своем начале  четное количество пикселей
 * \param *non_odd Указатель на переменную, содержащую количество фигур, имеющих в своем начале нечетное количество пикселей
 */
void search_for_squares(Mat img, int *odd, int *non_odd){
    int total = 0;
    Mat tmp;
    for(int i = 0; i < img.cols - 5; i++){
        for(int j = 0; j < img.rows - 5; j++){
            tmp = img(Rect(i, j, 4, 4));
            if(check_if_odd_square(tmp) == true) (*odd)++;
            else{
                tmp = img(Rect(i, j, 6, 6));
                if (check_if_non_odd_square(tmp) == true) (*non_odd)++;
            }
        }
    }
}
/*!
 * \brief Обрабатывает изображение, вызывая в нужной последовательности функции
 * \param argc Количество аргументов командной строки
 * \param argv Аргументы командной строки
 * \return 0 В случае успешного завершения работы программы.
 */
int main(int argc, char *argv[]){
    
    int particles[2048] = {0};
    Mat img, dst;
    int max_binary_value = 255, kernel_window = 1, kernel_type = 0, size = 0, tmp_odd = 0, tmp_non_odd = 0;
    Mat kernel = getStructuringElement( kernel_type,
                   Size( 2 * kernel_window + 1, 2 * kernel_window + 1 ),
                   Point( kernel_window , kernel_window ) );

    img = imread(argv[1], IMREAD_GRAYSCALE);
    threshold(img, dst, 0, max_binary_value, THRESH_BINARY | THRESH_OTSU);
    medianBlur(dst, dst, 7);
    while( !check_if_empty(dst) && ++size){
        tmp_odd = 0;
        tmp_non_odd = 0;
        dilate(dst, dst, kernel);
        search_for_squares(dst, &tmp_odd, &tmp_non_odd);
        particles[(size + 2) * 2  + 1] += tmp_non_odd;
        particles[(size + 2) * 2] += tmp_odd;
    }
    for(int i = 0; i < 2048; i++){
        if(particles[i] != 0) fprintf(stdout, "%d:%d\n", i, particles[i]);
    }
    return 0;
}
